-e .

black==23.1.0
bs4==0.0.1
category_encoders==2.6.0
click==8.1.3
fastcore==1.5.28
hdbscan==0.8.29
isort==5.12.0
jupyter==1.0.0
matplotlib==3.7.1
missingno==0.5.2
nbdev==2.3.12
numpy==1.23.5
pandas==1.5.3
ppscore==1.3.0
pre-commit==3.2.0
pyarrow==11.0.0
pytest==7.2.2
pyupgrade==3.3.1
requests==2.28.2
sklearn==0.0.post1
spacy==3.5.1
umap-learn==0.5.3
