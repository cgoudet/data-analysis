import numpy as np
import pandas as pd
import requests

from src import INTDIR, RAWDIR
from src.data.common import reduce_int_type


def read_raw():
    """
    Run this function to download all raw files from the website.
    """
    datadir = RAWDIR / "meteo_france"
    datadir.mkdir(exist_ok=True)
    dates = pd.date_range(start="2010-01-01", end="2021-01-01", freq="1m")
    for dt in dates:
        dt = dt.strftime("%Y%m")
        r = requests.get(
            f"https://donneespubliques.meteofrance.fr/donnees_libres/Txt/Synop/Archive/synop.{dt}.csv.gz"
        )
        assert r.status_code == 200
        with open(datadir / f"meteo_{dt}.csv", "wb") as f:
            f.write(r.content)


def read_dataset():
    datadir = RAWDIR / "meteo_france"
    all_files = list(datadir.glob("*.csv"))
    df = pd.concat(
        [pd.read_csv(x, sep=";", encoding="latin-1") for x in all_files],
        ignore_index=True,
    )
    return df


def adapt_types(df):
    df[df == "mq"] = np.nan
    df["date"] = pd.to_datetime(df["date"].astype(str), format="%Y%m%d%H%M%S")
    to_float = ["t", "pmer", "tend", "td"]
    df[to_float] = np.float32(df[to_float])

    to_int = ["cod_tend"]
    df[to_int] = df[to_int].fillna(-1).astype(np.int)
    return df


def drop_cols(df):
    to_keep = ["numer_sta", "t", "Nom", "date"]
    df = df[to_keep]
    return df


def add_postes_info(df):
    postes = pd.read_csv(RAWDIR / "postesSynop.csv", sep=";", encoding="latin1").rename(
        columns={"ID": "numer_sta"}
    )
    df = pd.merge(df, postes, on=["numer_sta"])
    return df


def pipeline():
    df = (
        read_dataset()
        .pipe(add_postes_info)
        .pipe(adapt_types)
        .pipe(drop_cols)
        .pipe(reduce_int_type)
    )
    df.to_parquet(INTDIR / "temperatures.parquet")
