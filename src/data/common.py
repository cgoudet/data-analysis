import re

import numpy as np
import pandas as pd
from fastcore.basics import ifnone


def reduce_int_type(df):
    int_cols = list(df.select_dtypes("integer").columns)
    encodings = [np.int8, np.int16, np.int32]
    for c in int_cols:
        m = np.abs(df[c]).max()
        for e in encodings:
            if m < np.iinfo(e).max:
                df[c] = e(df[c])
                break
    return df


def add_datepart(df, field_name, prefix=None, drop=True, time=False):
    """Helper function that adds columns relevant to a date in the column `field_name` of `df`.
    Code stolen from https://docs.fast.ai/tabular.core.html#add_datepart."""
    make_date(df, field_name)
    field = df[field_name]
    prefix = ifnone(prefix, re.sub("[Dd]ate$", "", field_name))
    attr = [
        "Year",
        "Month",
        "Week",
        "Day",
        "Dayofweek",
        "Dayofyear",
        "Is_month_end",
        "Is_month_start",
        "Is_quarter_end",
        "Is_quarter_start",
        "Is_year_end",
        "Is_year_start",
    ]
    if time:
        attr = attr + ["Hour", "Minute", "Second"]
    # Pandas removed `dt.week` in v1.1.10
    week = (
        field.dt.isocalendar().week.astype(field.dt.day.dtype)
        if hasattr(field.dt, "isocalendar")
        else field.dt.week
    )
    for n in attr:
        df[prefix + n] = getattr(field.dt, n.lower()) if n != "Week" else week
    mask = ~field.isna()
    df[prefix + "Elapsed"] = np.where(
        mask, field.values.astype(np.int64) // 10 ** 9, np.nan
    )
    if drop:
        df.drop(field_name, axis=1, inplace=True)
    return df


def make_date(df, date_field):
    "Make sure `df[date_field]` is of the right date type."
    field_dtype = df[date_field].dtype
    if isinstance(field_dtype, pd.core.dtypes.dtypes.DatetimeTZDtype):
        field_dtype = np.datetime64
    if not np.issubdtype(field_dtype, np.datetime64):
        df[date_field] = pd.to_datetime(df[date_field], infer_datetime_format=True)
