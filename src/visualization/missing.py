import matplotlib.pyplot as plt
import numpy as np
from sklearn.impute import KNNImputer


def simulate_missing(data, missing="nmr", lower=1.5, rate=0.5, seed=43):
    np.random.seed(seed)
    rate_mask = np.random.binomial(n=1, p=0.5, size=len(data)).astype(bool)
    if missing == "nmr":
        return (data[:, 1] > lower) & rate_mask
    elif missing == "mar":
        return (data[:, 0] > lower) & rate_mask
    elif missing == "mcar":
        return rate_mask
    else:
        raise KeyError(missing)


def compare_dist(d1, d2, opts={}, title=""):

    rms = (d1 - d2) * (d1 - d2)
    rms = rms[rms > 0].mean()
    fig, ax = plt.subplots()
    ax.hist(d1, **opts)
    ax.hist(d2, **opts)
    ax.set_title(title)
    if not np.isnan(rms):
        ax.text(-4, ax.get_ylim()[1] * 0.8, f"RMS : {rms:.2f}")


def compare_missing_values(data, missing="nmr"):
    opts = dict(bins=np.linspace(-5, 5, 51), alpha=0.5)
    mask = simulate_missing(data, missing)

    empty_data = data.copy()
    empty_data[mask, 1] = np.nan

    print("Shift in mean related to nmr")
    print(data[:, 1].mean(), np.nanmean(empty_data[:, 1]))

    compare_dist(
        data[:, 1],
        empty_data[:, 1],
        opts,
        "Distribution comparison with missing values",
    )

    fill0 = np.where(np.isnan(empty_data), 0, empty_data)
    compare_dist(
        data[:, 1], fill0[:, 1], opts, "Distribution comparison with 0 imputing"
    )

    imputer = KNNImputer(n_neighbors=2)
    fillknn = imputer.fit_transform(empty_data)
    compare_dist(
        data[:, 1], fillknn[:, 1], opts, "Distribution comparison with kNN imputing"
    )

    fillrandom = empty_data.copy()
    fillrandom[:, 1] = np.where(
        np.isnan(empty_data[:, 1]), np.random.normal(size=len(data)), empty_data[:, 1]
    )
    compare_dist(
        data[:, 1],
        fillrandom[:, 1],
        opts,
        "Distribution comparison with random imputing",
    )
